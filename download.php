<?php

function fail($message = '')
{
	$fail_message = $message;
	include('fail.php');
	die();
}

$proxied_headers = array('Set-Cookie', 'Content-Type', 'Cookie', 'Location');

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $_GET['url']);
curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

$res = curl_exec($ch);
curl_close($ch);

if($res == false)
	fail();

list($headers, $body) = explode("\r\n\r\n", $res, 2);

$headers = explode("\r\n", $headers);
$hs = array();

foreach($headers as $header)
{
    if(strpos($header, ':') != false)
    {
        list($h, $v) = explode(':', $header);
        $hs[$h][] = $v;
    }
    else
    {
        $header1 = $header; 
    }
}

if(strncmp($header1, 'HTTP', 4) != 0)
	fail('Server did not respond correctly');
else
{
	if(strcmp($header1, 'HTTP/1.1 200 OK') != 0 && strcmp($header1, 'HTTP/1.0 200 OK') != 0)
		fail('Server returned "'.$header1.'".');
}

foreach($proxied_headers as $hname)
{
    if(isset($hs[$hname]))
    {
        foreach($hs[$hname] as $v)
        {
            if($hname === 'Set-Cookie')
            {
                header($hname.": " . $v, false);
            }
            else
            {
                header($hname.": " . $v);
            }
        }
    }
}


if($_GET['content_type'] != '')
	header('Content-type: '.$_GET['content_type']);

$filename = $_GET['filename'];
if($filename == '')
	$filename = 'probably_not.html';

$content_disposition = $_GET['content_disposition'];
if($content_disposition != 'attachment' && $content_disposition != 'inline')
	$content_disposition = 'attachment';

header('Content-Disposition: '.$content_disposition.'; filename="'.$filename.'"');

die($body);
