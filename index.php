<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >

	<title>Download-Proxy</title>
</head>

<body>
	<h2>Download-Proxy</h2>
	There's always someone smarter than you.<br><br>

	<form action="download.php" method="get">
		<p>URL:<br><input name="url" type="text"></p>
		<p>Filename:<br><input name="filename" type="text" value="probably_not.html"></p>
		<p>Content-Type:<br><input name="content_type" type="text"> (leave empty if you don't want to change it)</p>
		<p>Content-Disposition:<br>
		<input type="radio" name="content_disposition" value="inline">inline (display file)<br>
		<input type="radio" name="content_disposition" value="attachment" checked>attachment (download file; might be blocked)</p>
		<p><input type="submit" value="Download"></p>
	</form>
	<br><br><br><br><br><br>
	<hr>
	
	<p style="text-align: right;">Download-Proxy by <a href="http://www.metallic-entertainment.com">Metallic Entertainment</a></p>

</body>

</html>

<? die(); // to remove Hosting24 Analytics Code ?>
